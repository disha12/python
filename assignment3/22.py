#a03p02a.py
l1=range(1,11)
print "l1 list is :",l1

#a03p02b.py
l2=range(1,101,10)
print "l2 list is :",l2 

#a03p02c.py
l3=['python','django','flask','string','function','classes']
print"l3 list is :",l3

#a03p02d.py
d1={'l1':l1}
d2={'l2':l2}
d3={'l3':l3}
l4=[d1,d2,d3]
print "l4 is a list of dictionaries:",l4

#a03p02e.py
main_list=[l1,l2,l3]
print"main_list is :",main_list

#a03p02f.py
l5=[l1*2]
print "list l5 is",l5

#a03p02g.py
main_list.append(l5)
print "appended l5 to main_list is :",main_list

z=main_list.count(1)
print"occurrences of integer 1 in main list is : ",z
